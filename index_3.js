// Classes, this keyword

// First of all - Object

var obj = {

    getName: function () { return 'Sam'; },
    getYear: function () { return '2015'; }

}

// Accessing properties

obj.getName();      // Dot way

obj['getYear']();   // Array way


// What is a Class

typeof Array;
typeof Object;
typeof String;
// ...


// What is an Instance of Class

var arr = new Array(),
    obj = new Object(),
    str = new String();

// Let's check Instances

arr instanceof Array;
obj instanceof Object;
str instanceof String;

// Try to create Class

var BaseClass = function () {},
    classInstance = new BaseClass();

classInstance instanceof BaseClass;

// Detail look

BaseClass.prototype;
BaseClass.prototype.constructor;

// The very basic example

typeof Array.prototype;

// As it is an object - we can use props

typeof Array.prototype.splice;

// Some fun
// var slicer = new Array.prototype.slice();

// See

var a = new Function();

typeof a;
typeof a.prototype;

// Custom class with custom prototype

var BaseClass = function () {};

typeof BaseClass.prototype;

BaseClass.prototype.getName = function () { return 'Sam'; };
BaseClass.prototype.getYear = function () { return '2015'; };

var classInstance = new BaseClass();

// Check inherited props

typeof classInstance.getName();
typeof classInstance.getYear();

typeof classInstance.prototype;

// Custom extend function

var extend = function (parent, protoProps) {

    var func = parent.prototype.constructor;

    for (var i in protoProps) {

        func.prototype[i] = protoProps[i];

    }

    // func.__super__ = parent.prototype;

    return func;

};

// Let's check

var MainClass = extend(BaseClass, {

    getSurname: function () { return 'Dark'; },
    getFullName: function () { return 'Sam Dark'; }

});

// This keyword

var obj = {

    name: 'Sam',

    getName: function () { return this.name; },

    describe: function () { console.log(this); }

};

// Remember this one?

typeof MainClass.prototype;

// Kinda same thing

var obj = {

    constructor: function () {},
    somefunction: function () {}
    // ...

}

var fnc = function () {};

// What's inside

typeof fnc.prototype; // is object, same as above

// Use the prototype object with this

var DefaultClass = function () { console.log(this); };

DefaultClass.prototype.name = 'Sam';
DefaultClass.prototype.log = function () { console.log(this) };
DefaultClass.prototype.getName = function () { return this.name; };

// This keyword behaves the same as in Object

var ins = new DefaultClass();

ins.log();
ins.getName();

// Scopes again

this;

// Simple bind

function () {

    console.log(this.name)

}.bind({ name: 'Sam' });

// Complicated bind

var check = (function () {

    return this;

}.bind( DefaultClass.prototype ))();

//

// Tricky chicky

var hiddenGetter = (function (props) {

    var hiddenProps = props;

    return {

        getProp: function (name) {

            return hiddenProps[name];

        },
        setProp: function (name, value) {

            hiddenProps[name] = value;
            return this;

        },
        getName: function () {

            return this.getProp('name')

        },
        setName: function (value) {

            return this.setProp('name', value);

        }

    }

}) ({ name: null, surname: null, year: null });






































//
