// Variables,
// Functions,
// Scopes,
// Falsy - Truthy,
// Math, Logic
// Constructions,
// Classes

// Variables
var int = 1;                                // Number, Object
var flt = 3.14,                             // Also a Number and Object
    str = "hello world",                    // String, Object
    arr = [ 1, 2, 'string', null ],         // Array, Object
    obj = {
        prop : 123,
        'newprop' : 124
    },  // Object
    bln = false || true,                    // Boolean, Object
    fnc = function () { return 123; },      // Function, Object
    rgx = /asdas/                           // RegExp, Object
    nul = null,                             // Object
    udf = undefined;                        // undefined

// Instance checks

// Boxed types
var isArrayArray    = arr instanceof Array,
    isArrayObject   = arr instanceof Object;

var isObjectObject  = obj instanceof Object;
    isObjectArray   = obj instanceof Array;

// ...

// Unboxed types
var isNumberNumber  = int instanceof Number,
    isBoolBoolean   = bln instanceof Boolean,
    isStringString  = str instanceof String;















// Functions

// Named functions
var f1 = function () { return true; };
function f2 () { return; };

// console.log(f2());

// Anonymous function
var result = ( function () {

     return function () {

     }

} )(  );

// Arguments
var f1 = function (x) { return x + 1; },
    f2 = function (x, y, z, h) { return x + y + 2; };

var res1 = f1(2),
    res2 = f2(1, '2');

// console.log(res2)

// Return arguments
var argumentsFunction = function () { return arguments; };

var args = argumentsFunction(1, 2, 3, 4),
    isArgumentsObject = args instanceof Object;

// console.log(args);












// A little bit of scopes
var someVariable = 'someValue';

var someFunction = function () {
    return someVariable;
},

someValue = someFunction();

// One more try

var i = 10;

var someFunction = function () {
    i = 10;
};

someFunction();

console.log(i);


// And one more
var someFunction = function () {
    var int = 123;
    var str = 'localFunctionString';
};

someFunction();

// Last trick
var x = [];

for (var i = 0; i < 3; i++)  {
    x[i] = function () { return i; };
}

console.log(
    x[0](),
    x[1](),
    x[2]()
);





// Falsy and Truthy values
var falsyCondition  = null || false || 0 || undefined || NaN || '',
    truthyCondition = true || 'foo' || 1 || [] || {};


// Always true statements
//
// console.log(null == undefined);
// console.log(false == 0);
// console.log(false == '');
// console.log('' == 0);
// console.log(true == 1);
// console.log(true == '1');
// console.log('1' == 1);
//
// Never use ==, better ===

// True story
// console.log(true == 'true')


// console.log(null + [1, 2])            // => ’null1,2’
// console.log(undefined + [1, 2])       // => ’undefined1,2’
// console.log(3 + {})                   // => ’3[object Object]’
// console.log(’’ + true)                // => ’true’
//
// All of these are numbers:
//
// console.log(undefined + undefined)    // => NaN
// console.log(undefined + null)         // => NaN
// console.log(null + null)              // => 0
// console.log({} + {})                  // => NaN
// console.log(true + true)              // => 2
// console.log(false + true)             // => 1

// console.log({})                       // truthy
// console.log(!!{})                     // coerce to boolean, truthy
// console.log(+{})                      // coerce to number, NaN, which is falsy
// console.log([])                       // truthy
// console.log(!![])                     // coerce to boolean, truthy
// console.log(+[])                      // coerce to number, 0, which is falsy
// console.log([] == false)              // true (because [] is really zero, or something)
// console.log([] == 0)                  // true
// console.log([] == ’’)                 // true (because 0 == ’’)
// console.log([] == [])                 // false (different references, no coercion)
// console.log([1] == [1])               // false (different references, no coercion)
// console.log([1] == +[1])              // true (right-hand side is number, coercion)












// Math operators

var a = 1;

    a = a + 4;      // 5
    a += 1;         // 6

    a = a - 1;      // 5
    a -= 1;         // 4

    a = a * 2;      // 8
    a *= 2          // 16

    a = a / 2;      // 8
    a /= 1;         // 8

    a = a % 5;      // 3
    a %= 2;         // 1







//
