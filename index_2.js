// Constructions

// Block (the simpliest one)

{
    var x = 2;
}

var condition = true;

// If-else

if (condition) {

} else {

}

// Two together

var otherCondition = false;

if (condition) {

} else if (otherCondition) {

} else {

}

// Ternary

condition ? true : false;

// Switch

var num = 6;

switch (num) {

    case 5  : console.log(5); break;

    case 6  : console.log(6);

    case 7  : console.log(7); break;

    default : console.log(0);

}

// Double condition

switch (num) {

    case 5:
    case 6: console.log(5, 6); break;

    default: console.log(0);

}

// For Loop

var a = new Array(10);

for (var i = 0; i < 10; i++) {
    // console.log(i, a[i]);
}

// For In Loop

var a = { a : 1, b : 2 };

for (var i in a) {
    // console.log(i, a[i]);
}

// Break, continue

// console.log('for switch');

for (var i = 0; i < 10; i++) {

    switch (i) {
        case 3: continue; break;
        case 4: break; break;
    }

    if (i == 7) continue;
    if (i == 8) break;

    // console.log(i);
}

// While

var i = 0;

while (i < 10) {
    i++;
}

i = 0;

do { console.log(i++); } while (i < 10);

// Label

block:
{
    var a = 2;
    var b = 3;
    var c = 4;
}

console.log(a, b, c);

// Break label

block: {
    var d = 2;
    var e = 3;

    break block;

    var f = 4;
}

console.log(d, e, f);


mainLoop:
    for (var i = 0; i < 10; i++) {

        secondaryLoop:
            for (var j = 0; j < 10; j++) {

                if (i == 0) {
                    continue secondaryLoop;
                }

                if (i == 1 && j == 7) {
                    break mainLoop;
                }

                // console.log(i, j)

            } // End secondary loop

    } // End main loop

// Back to the future

loop:
for (var i = 0; i < 10; i++) {

    switch (i) {
        case 3: continue loop; break;
        case 4: break loop; break;
    }

    if (i == 7) continue;
    if (i == 8) break;

}

// Back to scopes, with

var scope = {

    someParam : 'someValue',

    log : function () { console.log.apply(null, arguments); }
}

with (scope) {

    log(someParam);

    console.log(this);

}

// Out of scope

console.log(typeof log);

// Throw expression
// var a = new EvalError('aaa');
// throw a;

// throw new Error();
// throw new EvalError();
// throw new TypeError();
// throw new SyntaxError();
// throw new RangeError();
// throw new ReferenceError();
// throw new InternalError();
// throw new URIError(); // decodeURI

// Must have (always)
try {

}
// Optionally (on error)
catch (e) {

}
// Optionally (always)
finally {

}

// Bonus
// 100/3|0
