// Argument with snippetId from console
var snippetId = process.argv[2];

// All the snippets wrapped into object
var snippets = {

    'snippet-1' : function (skip) {

        if (!skip) console.log('\n snippet-1 \n');

        var varList = [

            12,             // number (+ infinity, + NaN)
            1.2,            // number
            true,           // boolean
            'text',         // string
            [1, 2, 3],      // array
            { a : 1 },      // object
            function () {}, // function
            /[a-z0-9]+\-/,  // regexp
            null,           // null
            undefined,      // undefined

        ];

        if (!skip) for (var i in varList) console.log(varList[i], typeof varList[i]);

        return varList;
    },

    'snippet-2' : function () {

        console.log('\n snippet-2 \n');

        // Getting values from previous example
        var varList = this['snippet-1'](true);

        // Remove null (last one)
        varList = varList.slice(0, 8);

        var prototypeLists = [

            Number,
            Number,
            Boolean,
            String,
            Array,
            Object,
            Function,
            RegExp

        ];


        for (var i in varList)
            console.log('\n ' + varList[i] + ' instanceof ' + prototypeLists[i], varList[i] instanceof prototypeLists[i], '\n');

        console.log('\n constructors\n');

        for (var i in varList)
            console.log('\n ' + varList[i].constructor, '\n');
    },

    'snippet-3' : function () {

        console.log('\n snippet-3 \n');

        // Falsy values

        if ( null || false || 0 || undefined || NaN || '' ) { } else console.log(' nothing is true \n')

        // Truthy values

        if ( true || 'foo' || 1 || [] || {} ) { console.log(' everything is true \n') }

        // Always true statements
        //
        // null == undefined
        // false == 0
        // false == ’’
        // ’’ == 0
        // true == 1
        // true == ’1’
        // ’1’ == 1

    },

    'snippet-4' : function () {

        console.log('\n snippet-4 \n');

        var a = 1;      // 1

        a = a + 4;      // 5

        a += 1;         // 6

        a = a - 1;      // 5

        a -= 1;         // 4

        a = a * 2;      // 8

        a *= 2          // 16

        a = a / 2;      // 8

        a /= 1;         // 8

        a = a % 5;      // 3

        a %= 2;         // 1

        console.log(' a =', a, '\n');

        var b = 'hello';

        b = b + ' world';

        b += '!';

        console.log(' b =', b, '\n');

        b -= '!';

        console.log(' b =', b, '\n');

    },

    'snippet-5' : function () {



    },

    'snippet-6' : function () {

        console.log('\n snippet-6 \n');

        var arr = [ 1, 2, 3 ],
            obj = { a : 1, b : 2, c : 3 };

        // Accessing properties or array
        console.log(' arr =', arr, '; arr[0] =',     arr[0],     '\n');

        // Of object
        console.log(' obj =', obj, '; obj[\'b\'] =', obj['b'],   '\n');
        console.log(' obj =', obj, '; obj.b =',      obj.b,      '\n');

        // Walking through

        for ( var i = 0; i < arr.length; i++ ) {

            console.log(' i = '+i+'; arr[' + i + ']', ' = ', arr[i]);

        }

        // Using 'in'

        console.log('\n');

        for ( var i in obj ) {

            console.log(' i = '+ i + '; obj[' + i + ']', '=', obj[i]);

        }

        console.log('\n');

        for ( var i in arr ) {

            console.log(' i = '+i+'; arr[' + i + ']', '=', arr[i]);

        }

        console.log('\n');

    }

};

// Run requested function
(function (id) { snippets[ id ]() })(snippetId);
